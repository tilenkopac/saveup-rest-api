package rest;

import entities.Category;
import entities.PredefinedCategory;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/categories")
public class CategoryService {

    private static EntityManager em = Persistence.createEntityManagerFactory("primary-unit").createEntityManager();

    @GET
    @Path("/{category_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoryById(@PathParam("category_id") int categoryId) {
        em.getTransaction().begin();
        Category foundCategory = em.find(Category.class, categoryId);
        em.getTransaction().commit();

        if (foundCategory != null) {
            return Response.status(200).entity(foundCategory).build();
        } else {
            return Response.status(200).build();
        }
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPredefinedCategory(PredefinedCategory newCategory) {
        try {
            em.getTransaction().begin();
            em.persist(newCategory);
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(newCategory).build();
    }

}
