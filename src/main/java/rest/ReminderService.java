package rest;

import entities.Reminder;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/reminders")
public class ReminderService {

    private static EntityManager em = Persistence.createEntityManagerFactory("primary-unit").createEntityManager();

    @GET
    @Path("/{reminder_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReminderById(@PathParam("reminder_id") int reminderId) {
        em.getTransaction().begin();
        Reminder foundReminder = em.find(Reminder.class, reminderId);
        em.getTransaction().commit();

        if (foundReminder != null) {
            return Response.status(200).entity(foundReminder).build();
        } else {
            return Response.status(200).build();
        }
    }

}
