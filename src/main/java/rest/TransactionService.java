package rest;

import entities.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/transactions")
public class TransactionService {

    private static EntityManager em = Persistence.createEntityManagerFactory("primary-unit").createEntityManager();

    @GET
    @Path("/{transaction_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransactionById(@PathParam("transaction_id") int transactionId) {
        em.getTransaction().begin();
        Transaction foundTransaction = em.find(Transaction.class, transactionId);
        em.getTransaction().commit();

        if (foundTransaction != null) {
            return Response.status(200).entity(foundTransaction).build();
        } else {
            return Response.status(200).build();
        }
    }

}
