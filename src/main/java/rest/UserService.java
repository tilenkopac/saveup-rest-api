package rest;

import entities.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/users")
public class UserService {

    private static EntityManager em = Persistence.createEntityManagerFactory("primary-unit").createEntityManager();

    @GET
    @Path("/{user_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserById(@PathParam("user_id") int userId) {
        em.getTransaction().begin();
        User foundUser = em.find(User.class, userId);
        em.getTransaction().commit();

        if (foundUser != null) {
            return Response.status(200).entity(foundUser).build();
        } else {
            return Response.status(200).build();
        }
    }

    @GET
    @Path("/{user_id}/transactions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserTransactions(@PathParam("user_id") int userId) {
        em.getTransaction().begin();

        User user = em.find(User.class, userId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Transaction> query = cb.createQuery(Transaction.class);
        Root<Transaction> root = query.from(Transaction.class);
        query.select(root).where(
                cb.equal(root.get("user"), user)
        ).orderBy(
                cb.asc(root.get("dateAndTime"))
        );
        List<Transaction> userTransactions = em.createQuery(query).getResultList();

        em.getTransaction().commit();

        if (!userTransactions.isEmpty()) {
            return Response.status(200).entity(userTransactions).build();
        } else {
            return Response.status(200).build();
        }
    }

    @GET
    @Path("/{user_id}/categories")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserCategories(@PathParam("user_id") int userId) {
        em.getTransaction().begin();
        List<CustomCategory> userCategories = em.find(User.class, userId).getCustomCategories();
        em.getTransaction().commit();

        if (userCategories != null) {
            return Response.status(200).entity(userCategories).build();
        } else {
            return Response.status(200).build();
        }
    }

    @GET
    @Path("/{user_id}/budgets")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserBudgets(@PathParam("user_id") int userId) {
        em.getTransaction().begin();
        List<Budget> userBudgets = em.find(User.class, userId).getBudgets();
        em.getTransaction().commit();

        if (userBudgets != null) {
            return Response.status(200).entity(userBudgets).build();
        } else {
            return Response.status(200).build();
        }
    }

    @GET
    @Path("/{user_id}/reminders")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserReminders(@PathParam("user_id") int userId) {
        em.getTransaction().begin();
        List<Reminder> userReminders = em.find(User.class, userId).getReminders();
        em.getTransaction().commit();

        if (userReminders != null) {
            return Response.status(200).entity(userReminders).build();
        } else {
            return Response.status(200).build();
        }
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(User newUser) {
        try {
            em.getTransaction().begin();
            em.persist(newUser);
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }
        return Response.status(200).entity(newUser).build();
    }

    @POST
    @Path("{user_id}/transactions/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTransaction(@PathParam("user_id") int userId, Transaction newTransaction) {
        try {
            em.getTransaction().begin();
            User user = em.find(User.class, userId);
            newTransaction.setUser(user);

            em.persist(newTransaction.getCategory());
            if (newTransaction.getLocation() != null) {
                em.persist(newTransaction.getLocation());
            }

            em.persist(newTransaction);
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }
        return Response.status(200).entity(newTransaction).build();
    }

    @POST
    @Path("{user_id}/categories/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomCategory(@PathParam("user_id") int userId, CustomCategory newCategory) {
        try {
            em.getTransaction().begin();
            User user = em.find(User.class, userId);
            newCategory.setUser(user);
            em.persist(newCategory);
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }
        return Response.status(200).entity(newCategory).build();
    }

    @POST
    @Path("{user_id}/budgets/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createBudget(@PathParam("user_id") int userId, Budget newBudget) {
        try {
            em.getTransaction().begin();
            User user = em.find(User.class, userId);

            // if budget type already exists, replace values
            boolean budgetTypeAlreadyExists = false;
            for (Budget budget : user.getBudgets()) {
                if (budget.getType().equals(newBudget.getType())) {
                    budgetTypeAlreadyExists = true;
                    budget.setSize(newBudget.getSize());
                    budget.setUserExpenses(0);
                }
            }
            if (!budgetTypeAlreadyExists) {
                newBudget.setUser(user);
                newBudget.setUserExpenses(0);
                em.persist(newBudget);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }
        return Response.status(200).entity(newBudget).build();
    }

    @POST
    @Path("{user_id}/reminders/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createReminder(@PathParam("user_id") int userId, Reminder newReminder) {
        try {
            em.getTransaction().begin();
            User user = em.find(User.class, userId);
            newReminder.setUser(user);
            em.persist(newReminder);
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }
        return Response.status(200).entity(newReminder).build();
    }

    @PUT
    @Path("{user_id}/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerUser(@PathParam("user_id") int userId, User updatedUser) {
        try {
            em.getTransaction().begin();
            User user = em.find(User.class, userId);
            user.setUuid(null);
            user.setEmail(updatedUser.getEmail());
            user.setPassword(updatedUser.getPassword());
            user.setFirstName(updatedUser.getFirstName());
            user.setLastName(updatedUser.getLastName());
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }
        return Response.status(200).entity(updatedUser).build();
    }

    @DELETE
    @Path("{user_id}/reminders/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUserReminder(@PathParam("user_id") int userId) {
        try {
            em.getTransaction().begin();
            User user = em.find(User.class, userId);
            user.setReminders(new ArrayList<Reminder>());
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.toString()).build();
        }
        return Response.status(200).build();
    }

}
