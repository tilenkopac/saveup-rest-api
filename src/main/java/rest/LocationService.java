package rest;

import entities.Location;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/locations")
public class LocationService {

    private static EntityManager em = Persistence.createEntityManagerFactory("primary-unit").createEntityManager();

    @GET
    @Path("/{location_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLocationById(@PathParam("location_id") int locationId) {
        em.getTransaction().begin();
        Location foundLocation = em.find(Location.class, locationId);
        em.getTransaction().commit();

        if (foundLocation != null) {
            return Response.status(200).entity(foundLocation).build();
        } else {
            return Response.status(200).build();
        }
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createLocation(Location newLocation) {
        try {
            em.getTransaction().begin();
            em.persist(newLocation);
            em.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(newLocation).build();
    }

}
