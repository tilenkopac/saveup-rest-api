package rest;

import entities.Budget;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/budgets")
public class BudgetService {

    private static EntityManager em = Persistence.createEntityManagerFactory("primary-unit").createEntityManager();

    @GET
    @Path("/{budget_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBudgetById(@PathParam("budget_id") int budgetId) {
        em.getTransaction().begin();
        Budget foundBudget = em.find(Budget.class, budgetId);
        em.getTransaction().commit();

        if (foundBudget != null) {
            return Response.status(200).entity(foundBudget).build();
        } else {
            return Response.status(200).build();
        }
    }

}