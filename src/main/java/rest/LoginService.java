package rest;

import entities.User;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/login")
public class LoginService {

    private static EntityManager em = Persistence.createEntityManagerFactory("primary-unit").createEntityManager();

    @POST
    @Path("/email")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginWithEmail(User userLoggingIn) {
        // check if email exists in database
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(
                cb.equal(root.get("email"), userLoggingIn.getEmail())
        );
        List<User> foundUsers = em.createQuery(query).getResultList();
        if (!foundUsers.isEmpty()) {
            User foundUser = foundUsers.get(0);
            // todo: (maybe) add password check
            return Response.status(200).entity(foundUser).build();
        } else {
            return Response.status(400).entity("User does not exist").build();
        }
    }

    @POST
    @Path("/uuid")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginWithUuid(User userLogingIn) {
        // check if uuid exists in database
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(
                cb.equal(root.get("uuid"), userLogingIn.getUuid())
        );
        List<User> foundUsers = em.createQuery(query).getResultList();
        if (!foundUsers.isEmpty()) {
            User foundUser = foundUsers.get(0);
            return Response.status(200).entity(foundUser).build();
        } else {
            return Response.status(400).entity("User does not exist").build();
        }
    }

}
