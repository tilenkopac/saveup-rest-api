package entities.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransactionTypeValidator implements ConstraintValidator<ValidateTransactionType, String> {

    private List<String> validValues;

    public void initialize(ValidateTransactionType constraintAnnotation) {
        validValues = new ArrayList<String>();
        validValues.addAll(Arrays.asList("income", "expense"));
    }

    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || validValues.contains(value);
    }

}
