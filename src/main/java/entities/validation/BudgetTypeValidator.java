package entities.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BudgetTypeValidator implements ConstraintValidator<ValidateBudgetType, String> {

    private List<String> validValues;

    public void initialize(ValidateBudgetType constraintAnnotation) {
        validValues = new ArrayList<String>();
        validValues.addAll(Arrays.asList("daily", "weekly", "monthly"));
    }

    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || validValues.contains(value);
    }

}
