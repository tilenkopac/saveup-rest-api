package entities.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TransactionTypeValidator.class)
@Documented
public @interface ValidateTransactionType {

    String message() default "Transaction type not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
