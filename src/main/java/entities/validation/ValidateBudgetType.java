package entities.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BudgetTypeValidator.class)
@Documented
public @interface ValidateBudgetType {

    String message() default "Budget type not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
