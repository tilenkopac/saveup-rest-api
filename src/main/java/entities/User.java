package entities;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Size(min = 36, max = 36)
    @Column(name = "user_uuid")
    private String uuid;

    @Email
    @Column(name = "user_email")
    private String email;

    @Column(name = "user_password")
    private String password;

    @Size(max = 30)
    @Column(name = "user_first_name")
    private String firstName;

    @Size(max = 30)
    @Column(name = "user_last_name")
    private String lastName;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Transaction> transactions = new ArrayList<Transaction>();

    @OneToMany(mappedBy = "user")
    private List<CustomCategory> customCategories = new ArrayList<CustomCategory>();

    @OneToMany(mappedBy = "user")
    private List<Budget> budgets = new ArrayList<Budget>();

    @OneToMany(mappedBy = "user")
    private List<Reminder> reminders = new ArrayList<Reminder>();

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", transactions=" + transactions +
                ", customCategories=" + customCategories +
                ", budgets=" + budgets +
                ", reminders=" + reminders +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
        if (transaction.getUser() != this) {
            transaction.setUser(this);
        }
    }

    public void removeTransaction(Transaction transaction) {
        transactions.remove(transaction);
        if (transaction.getUser() == this) {
            transaction.setUser(null);
        }
    }

    public List<CustomCategory> getCustomCategories() {
        return customCategories;
    }

    public void setCustomCategories(List<CustomCategory> customCategories) {
        this.customCategories = customCategories;
    }

    public void addCustomCategory(CustomCategory customCategory) {
        customCategories.add(customCategory);
        if (customCategory.getUser() != this) {
            customCategory.setUser(this);
        }
    }

    public void removeCustomCategory(CustomCategory customCategory) {
        customCategories.remove(customCategory);
        if (customCategory.getUser() == this) {
            customCategory.setUser(null);
        }
    }

    public List<Budget> getBudgets() {
        return budgets;
    }

    public void setBudgets(List<Budget> budgets) {
        this.budgets = budgets;
    }

    public void addBudget(Budget budget) {
        budgets.add(budget);
        if (budget.getUser() != this) {
            budget.setUser(this);
        }
    }

    public void removeBudget(Budget budget) {
        budgets.remove(budget);
        if (budget.getUser() == this) {
            budget.setUser(null);
        }
    }

    public List<Reminder> getReminders() {
        return reminders;
    }

    public void setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
    }

    public void addReminder(Reminder reminder) {
        reminders.add(reminder);
        if (reminder.getUser() != this) {
            reminder.setUser(this);
        }
    }

    public void removeReminder(Reminder reminder) {
        reminders.remove(reminder);
        if (reminder.getUser() == this) {
            reminder.setUser(null);
        }
    }

}
