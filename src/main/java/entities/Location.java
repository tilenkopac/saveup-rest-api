package entities;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "locations")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id")
    private int id;

    @Size(max = 40)
    @Column(name = "location_address")
    private String address;

    @NotNull
    @Digits(integer = 2, fraction = 6)
    @Min(-90)
    @Max(90)
    @Column(name = "location_lat")
    private float latitude;

    @NotNull
    @Digits(integer = 3, fraction = 6)
    @Min(-180)
    @Max(180)
    @Column(name = "location_long")
    private float longitude;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "location")
    private List<Transaction> transactions = new ArrayList<Transaction>();

    public Location() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String adress) {
        this.address = adress;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    @JsonIgnore
    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
        if (transaction.getLocation() != this) {
            transaction.setLocation(this);
        }
    }

    public void removeTransaction(Transaction transaction) {
        transactions.remove(transaction);
        if (transaction.getLocation() == this) {
            transaction.setLocation(null);
        }
    }

}
