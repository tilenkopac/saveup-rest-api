package entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.validation.ValidateBudgetType;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "budgets")
public class Budget {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "budget_id")
    private int id;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @ValidateBudgetType
    @Column(name = "budget_type")
    private String type;

    @Min(value = 0)
    @Max(value = 1000000)
    @Column(name = "budget_size")
    private int size;

    @Min(value = 0)
    @Max(value = 2000000)
    @Column(name = "budget_user_expenses")
    private int userExpenses;

    public Budget() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        if (!user.getBudgets().contains(this)) {
            user.getBudgets().add(this);
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getUserExpenses() {
        return userExpenses;
    }

    public void setUserExpenses(int user_expenses) {
        this.userExpenses = user_expenses;
    }
}
